#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thurs Nov 30 18:00:01 2017

@author: Benjamin Hartley

Extracts trivia questions from a csv file and posts them to an http address
arg1: trivia question data csv file
arg2: output JSON file
"""

import sys, os, argparse
import csv
import json

## Parse input and output arguments
parser = argparse.ArgumentParser()
parser.add_argument('infile', help='file to extract data from', type=str)
parser.add_argument('outfile', help='file name to save converted data to', type=str)
args = parser.parse_args()

infile = args.infile
outfile = args.outfile

## Make sure specified input file is available
if os.path.exists(infile):
    print('Input file found')
else:
    print('Input file not found')
    sys.exit()

## Open file to read
with open(infile, 'r') as inf:
    field_history = 0
    field_question = 0
    field_answer_one = 0
    field_answer_two = 0
    field_correct_answer = 0

    reader = csv.reader(inf, delimiter=',', quotechar='"')
    with open(outfile, 'w') as outf:
        temp = []
        for index, line in enumerate(reader):
            if index == 0:
                ## Configure these for different types of input files
                ## Fields here should match the input file exactly                
                for x in range(0, len(line)):
                    if 'History' in line[x]:
                        field_history = x
                    elif 'Question' in line[x]:
                        field_question = x
                    elif 'Answer 1' in line[x]:
                        field_answer_one = x
                    elif 'Answer 2' in line[x]:
                        field_answer_two = x
                    elif 'Correct Answer' in line[x]:
                        field_correct_answer = x
                continue
            elif not line[field_question]:
                print('question field empty, skipping')
                continue

            ## Prepare the question object to load onto the temp list
            body = {}
            body['category'] = 'history'
            body['question'] = line[field_question]
            body['answerOne'] = line[field_answer_one]
            body['answerTwo'] = line[field_answer_two]
            if line[field_answer_one] in line[field_correct_answer]:
                body['answerOneCorrect'] = True
            else:
                body['answerOneCorrect'] = False
    
            temp.append(body)
        ## Write formatted data to output file as JSON
        outf.writelines(json.dumps(temp))
    print('Done writing data')
