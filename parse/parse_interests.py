# -*- coding: utf-8 -*-

import sys, os, re, json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('infile',
                    help='csv file with user data',
                    type=str)
parser.add_argument('outfile',
                    help='output file name',
                    type=str)
args = parser.parse_args()
infile = args.infile
outfile = args.outfile

## Look for specified input file
if os.path.exists(infile):
    print('Input file found')
else:
    print('Input file not found')
    sys.exit()

with open(infile, 'r') as inf:
    ## Read the first line of the file, split by commas and create a list
    first = inf.readline().lower()
    first = first.split(",")
    fieldindexes = []
    
    ## Search for the given field
    for x in range(0, len(first)):
        if first[x].find('Broad Category') != -1:
            fieldindexes.append(x)
        elif first[x].find('Narrow Category') != -1:
            fieldindexes.append(x)
        elif first[x].find('Defined Category') != -1:
            fieldindexes.append(x)

    print(fieldindexes)
