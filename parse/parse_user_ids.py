#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 15:30:05 2018

@author: ben
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 11 10:40:01 2017

@author: ben

Extracts a single field from all records and writes them to the specified location
arg1: field to extract
arg2: user data csv file
arg3: output file
"""

import sys, os, re, json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('infile',
                    help='csv file with user data',
                    type=str)
parser.add_argument('outfile',
                    help='output file name',
                    type=str)
args = parser.parse_args()
infile = args.infile
outfile = args.outfile

## Look for specified input file
if os.path.exists(infile):
    print('Input file found')
else:
    print('Input file not found')
    sys.exit()

with open(infile, 'r') as inf:
    ## Read the first line of the file, split by commas and create a list
    first = inf.readline().lower()
    first = first.split(",")
    fieldindexes = []
    
    ## Search for the given field
    for x in range(0, len(first)):
        if first[x].find('user_id') != -1:
            fieldindexes.append(x)

    if len(fieldindexes) == 0:
        print('Field not found\n')
        sys.exit()
    elif len(fieldindexes) > 1:
        print('Please be more specific, found multiple matches:\n')
        for y in range(0, len(fieldindexes)):
            print(first[fieldindexes[y]])
        sys.exit()

    users = []
    with open(outfile, 'w') as outf:
        print('Start writing data')
        line_words = (line.split(',') for line in inf if ',' in line)
        for words in line_words:
            if len(words) > 1:
                users.append(re.sub('"', '', words[fieldindexes[0]].strip()))
#        outf.writelines(re.sub('"', '', words[fieldindexes[0]].strip()) + '\n' for words in line_words if len(words) > 1)
        print(users)
        outf.writelines(json.dumps(users))
    print('End writing data')
