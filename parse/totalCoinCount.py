#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 14:37:11 2017

@author: ben

"""

import sys, re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('infile',
                    help='csv file with user data',
                    type=str)
parser.add_argument('--verbose', help='print more data to screen',
                    action='store_true')
args = parser.parse_args()

infile = args.infile
v = args.verbose
field = 'coins'

with open(infile, 'r') as inf:
    first = inf.readline().split(",")
    fieldindex = -1
    print('\n')
    print('Scanning fields in file for: ' + field)
    for x in range(0, len(first)):
        if first[x].find(field) != -1:
            fieldindex = x
            print('Scan stopped. Now summing ' + first[x])
            break
    
    if fieldindex == -1:
        print(field + ' not found')
        sys.exit()

    totalCoins = 0
    usersWithZeroCoins = 0
    usersWithCoins = 0
    line_words = (line.split(',') for line in inf if ',' in line)

    for words in line_words:
        if len(words) > 1:
            coins = re.sub('"', '', words[fieldindex].strip())
            if coins == '':
                usersWithZeroCoins += 1
            else:
                usersWithCoins += 1
                totalCoins += int(coins)
                if v:
                    print(' --  running total: ' + str(totalCoins) + ' -- coins added from this user: ' + str(coins))
    print('\n')
    print('Final Count for ' + infile)
    if v:
        print('---------------------:------------')
        print('Users with no coins  :  ' + str(usersWithZeroCoins))
        print('Users with coins     :  ' + str(usersWithCoins))
        totalUsers = usersWithZeroCoins + usersWithCoins
        print('Total users          :  ' + str(totalUsers))
        print('---------------------:------------')
    print('TotalCoins           :  ' + str(totalCoins))
    if v:
        averageCoinsPerUser = int(totalCoins / totalUsers)
        print('Avg coins per user   :  ' + str(averageCoinsPerUser))
    print('\n')
