#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 18:10:24 2017

@author: ben
"""

from graph import Graph

graphX = Graph('black', 'yellow')

weekly_income = 820

monthly_costs = [
        65,  #phone
        100, #water
        60,  #internet
        60,  #power
        620, #rent
        390, #car
        100, #insurance
        60,  #gas
        300, #food
        200  #misc
        ]

months = [ #leap years ignored
        31,28,31,30,31,30,31,31,30,31,30,31
        ]

years_to_project = 2
days_to_project = years_to_project * 365
print('Projecting ' + str(years_to_project) + ' years, ' + str(days_to_project) + ' days')

day = 0
month = 0
month_of_year = 0
year = 0
monthly_income = 0
total_money = 0

x = 0
while x < days_to_project:
    day += 1
    if x % 7 == 0:
        monthly_income += weekly_income
        total_money += weekly_income
#        print('Income of ' + str(weekly_income) + ', total_money: ' + str(total_money))
    if day == months[month_of_year]:
        month += 1
        month_of_year += 1
        day = 0
        msg = 'After month ' + str(month) + ', monthly_income: ' + str(monthly_income) + ', total_money: ' + str(total_money)

        sum = 0
        for cost in monthly_costs:
            total_money -= cost
            sum += cost
        msg += '  -  monthly_costs: ' + str(sum)
        msg += '  =  total_money after costs: ' + str(total_money)

        adjX, adjY = -500, -500
        graphX.draw_circle((month * 40) + adjX, (total_money / 40) + adjY, 3, True)

        monthly_income = 0 # reset monthly income
        print(msg)
        if month % 12 == 0:
            year += 1
            month_of_year = 0
    x += 1