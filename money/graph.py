# -*- coding: utf-8 -*-

import turtle
turtle.hideturtle()

allowed_colors = [ 'red', 'orange', 'yellow',
                  'green', 'blue', 'purple',
                  'pink', 'white', 'black' ]

class Graph:
    def __init__(self, color_one = 'red', color_two = 'yellow', speed = 'max'):
        self.change_colors(color_one, color_two)

        self.screen = turtle.Screen()
        self.screen.onclick(turtle.goto, 1)
        self.screen.onclick(self.click, 1)
        self.screen.onclick(self.click_two, 3)

        if speed == 'high':
            self.delay = 0.5
            self.speed = 10
            turtle.speed(10)
        elif speed == 'medium':
            self.delay = 1
            self.speed = 6
            turtle.speed(6)
        elif speed == 'low':
            self.delay = 2
            self.speed = 3
            turtle.speed(3)
        else:
            self.delay = 0
            self.speed = 0
            turtle.speed(0)

    def click(self, x, y):
        self.draw_square(x, y, 30, True)

    def click_two(self, x, y):
        orig_color_one = self.color_one
        orig_color_two = self.color_two
        tmp = self.color_two

        for color in allowed_colors:
            self.change_colors(tmp, color)
            self.draw_circle(x, y, 30, True)
            x += 5
            y += 5
            tmp = color

        self.color_one = orig_color_one
        self.color_two = orig_color_two
        self.change_colors(self.color_one, self.color_two)

    def change_colors(self, color_one, color_two):
        if color_one in allowed_colors:
            self.color_one = color_one
        else:
            self.color_one = 'red'
        if color_two in allowed_colors:
            self.color_two = color_two
        else:
            self.color_two = 'yellow'
        turtle.color(self.color_one, self.color_two)

    def set_position(self, x, y, h = 0):
        turtle.pu()
        turtle.setx(x)
        turtle.sety(y)
        turtle.seth(0)
        turtle.pd()

    def draw_square(self, x, y, length, fill = False):
        self.set_position(x, y)
        if fill == True:
            turtle.begin_fill()
        for i in range(0, 4):
            turtle.forward(length)
            turtle.left(90)
        if fill == True:
            turtle.end_fill()

    def draw_rectangle(self, x, y, length, height, fill = False):
        self.set_position(x, y)
        if fill == True:
            turtle.begin_fill()
        turtle.forward(length)
        turtle.left(90)
        turtle.forward(height)
        turtle.left(90)
        turtle.forward(length)
        turtle.left(90)
        turtle.forward(height)
        turtle.left(90)
        if fill == True:
            turtle.end_fill()

    def draw_circle(self, x, y, r, fill = False):
        self.set_position(x, y)
        if fill == True:
            turtle.begin_fill()
        turtle.circle(r)
        if fill == True:
            turtle.end_fill()

    def draw_equilateral_triangle(self, x, y, length, fill = False):
        self.set_position(x, y)
        if fill == True:
            turtle.begin_fill()
        for i in range(0, 3):
            turtle.forward(length)
            turtle.left(120)
        if fill == True:
            turtle.end_fill()
